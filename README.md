# Ubuntu Touch, Gitlab CI adaption for the OnePlus 5 (cheeseburger) and OnePlus 5T (dumpling)

### Flashing of devel-builds

It seems that the device allows one fastboot-action only. After that, it is needed to reboot to bootloader again, to do another action.
So, to flash boot.img and ubuntu.img:
1. run `fastboot flash boot boot.img` in terminal
2. boot device in bootloader mode and connect it do pc. Flashing should start.
3. disconnect device
4. run `fastboot flash system ubuntu.img` in terminal (I need to use `mfastboot` (motorola implementation), since somehow my normal `fastboot` throws errors on flashing system)
5. reboot device to bootloader and connect it to pc. Flashing should start.
